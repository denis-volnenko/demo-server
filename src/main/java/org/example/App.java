package org.example;

import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.xml.ws.Endpoint;
import java.util.Date;

/**
 * Hello world!
 *
 */

@WebService
public class App {

    @WebMethod
    public Date getDate() {
        return new Date();
    }

    public static void main( String[] args ) {
        final String wsdl = "http://0.0.0.0:8080/App?wsdl";
        Endpoint.publish(wsdl, new App());
        System.out.println(wsdl);
    }

}
