#!/bin/bash

echo 'SHUTDOWN DEMO-SERVER...';
kill -9 $(pgrep -f demo-server.jar)
echo 'OK';
