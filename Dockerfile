FROM java:8
MAINTAINER denis@volnenko.ru

ADD ./target/demo-server.jar .
EXPOSE 8080
ENTRYPOINT exec /usr/bin/java -jar ./demo-server.jar
