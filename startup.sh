#!/bin/bash

echo 'STARTING DEMO SERVER...'
nohup java -jar ./demo-server.jar 2> ds.err < /dev/null &
echo 'OK'